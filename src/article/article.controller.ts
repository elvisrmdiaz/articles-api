import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  Query,
  Req,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { Request } from 'express';
import { ArticleService } from './article.service';

@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get('/')
  async getArticlesFiltered(@Req() req: Request, @Res() res) {
    let options = {};
    if (req.query.searchString) {
      options = {
        $or: [
          { author: new RegExp(req.query.searchString.toString(), 'i') },
          { title: new RegExp(req.query.searchString.toString(), 'i') },
          { _tags: new RegExp(req.query.searchString.toString(), 'i') },
        ],
      };
    }
    const query = this.articleService.getArticlesFiltered(options);

    const page: number = parseInt(req.query.page as any) || 1;
    const limit = 5;
    const total = await this.articleService.count(options);

    const data = await query.skip((page - 1) * limit).limit(limit);
    res
      .status(HttpStatus.OK)
      .json({ data, total, page, last_page: Math.ceil(total / limit) });
  }

  @Delete('/')
  async deleteArticle(@Res() res, @Query('articleID') articleID) {
    if (!articleID) {
      throw new NotFoundException('Param ID not found');
    }
    const article = await this.articleService.deleteArticle(articleID);
    if (article) {
      res
        .status(HttpStatus.OK)
        .json({ message: 'Article Deleted Sucessfully', article });
    } else {
      throw new NotFoundException('Article Does Not Exists');
    }
  }
}
