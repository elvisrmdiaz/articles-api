import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { AxiosResponse } from 'axios';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Article } from './interfaces/article.interface';

import { map } from 'rxjs/operators';
@Injectable()
export class ArticleService {
  constructor(
    @InjectModel('Article') private articleModel: Model<Article>,
    private httpService: HttpService,
  ) {}

  async getArticles(options): Promise<Article[]> {
    const articles = await this.articleModel.find(options);
    return articles;
  }
  deleteArticle(articleID: string) {
    return this.articleModel.findByIdAndDelete(articleID);
  }
  getArticlesFiltered(options) {
    return this.articleModel.find(options);
  }
  count(options) {
    return this.articleModel.count(options).exec();
  }
  @Cron('0 * * * *')
  async handleCron() {
    const data = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(
        map((axiosResponse: AxiosResponse<any>) => {
          return axiosResponse.data;
        }),
      )
      .toPromise();

    const hits = data.hits;
    for (const article in hits) {
      if (hits.hasOwnProperty(article)) {
        const newArticle = new this.articleModel(hits[article]);
        await newArticle.save();
      }
    }
    return data;
  }
}
